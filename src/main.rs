
use std::env;
use std::process::Command;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <process_name>", args[0]);
        std::process::exit(1);
    }

    let process_name = &args[1];

    match pgrep(process_name) {
        Ok(output) => {
            if output.trim().is_empty() {
                println!("No processes found for: {}", process_name);
                return;
            }

            println!("Found processes:\n{}", output);
            
            if pkill(process_name).is_err() {
                eprintln!("Failed to kill the processes for: {}", process_name);
            } else {
                println!("Successfully killed the processes for: {}", process_name);
            }
        }
        Err(e) => {
            eprintln!("Error with pgrep: {}", e);
        }
    }
}

fn pgrep(process_name: &str) -> Result<String, String> {
    let output = Command::new("pgrep")
        .arg(process_name)
        .output()
        .map_err(|e| e.to_string())?;

    if output.status.success() {
        Ok(String::from_utf8_lossy(&output.stdout).trim().to_string())
    } else {
        Err(String::from_utf8_lossy(&output.stderr).trim().to_string())
    }
}

fn pkill(process_name: &str) -> Result<(), String> {
    let output = Command::new("pkill")
        .arg(process_name)
        .output()
        .map_err(|e| e.to_string())?;

    if output.status.success() {
        Ok(())
    } else {
        Err(String::from_utf8_lossy(&output.stderr).trim().to_string())
    }
}

